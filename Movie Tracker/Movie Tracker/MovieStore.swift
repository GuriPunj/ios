//
//  MovieStore.swift
//  Movie Tracker
//
//  Created by Gurpreet Singh Punj on 2017-12-16.
//  Copyright © 2017 Gurpreet Singh Punj. All rights reserved.
//

import UIKit

class MovieStore
{
    var allMovies = [Movie]()
    
    @discardableResult func createMovie() -> Movie {
        let newMovie = Movie(title: "", theatre: "", rating: 0, viewedDate: Date() )
        
        allMovies.append(newMovie)
        
        return newMovie
    }
    
    func removeMovie(_ movie: Movie) {
        if let index = allMovies.index(of: movie) {
            allMovies.remove(at: index)
        }
    }
    
    func moveMovie(from fromIndex: Int, to toIndex: Int) {
        if fromIndex == toIndex {
            return
        }
        
        // Get reference to object being moved so you can reinsert it
        let movedMovie = allMovies[fromIndex]
        
        // Remove item from array
        allMovies.remove(at: fromIndex)
        
        // Insert item in array at new location
        allMovies.insert(movedMovie, at: toIndex)
    }
    
    let movieArchiveURL: URL = {
        let documentsDirectories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = documentsDirectories.first!
        return documentDirectory.appendingPathComponent("movies.archive")
    }()
    
    func save() -> Bool {
        print("Saving items to: \(movieArchiveURL.path)")
        return NSKeyedArchiver.archiveRootObject(allMovies, toFile: movieArchiveURL.path)
    }
    
    init() {
        if let archivedItems = NSKeyedUnarchiver.unarchiveObject(withFile: movieArchiveURL.path) as? [Movie] {
            allMovies = archivedItems
        }
    }

}
