//
//  MovieViewController.swift
//  Movie Tracker
//
//  Created by Gurpreet Singh Punj on 2017-12-16.
//  Copyright © 2017 Gurpreet Singh Punj. All rights reserved.
//

import UIKit

class MovieViewController: UITableViewController
{
    var movieStore: MovieStore!
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieStore.allMovies.count
    }
    
    
    @IBAction func createNewMovie(_ sender: UIBarButtonItem) {
        // Create a new item and add it to the store
        let newMovie = movieStore.createMovie()
        
        // Figure out where that item is in the array
        if let index = movieStore.allMovies.index(of: newMovie) {
            let indexPath = IndexPath(row: index, section: 0)
            
            // Insert this new row into the table
            tableView.insertRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func tableView(_ tableView: UITableView,
                            commit editingStyle: UITableViewCellEditingStyle,
                            forRowAt indexPath: IndexPath) {
        // If the table view is asking to commit a delete command...
        if editingStyle == .delete {
            let movie = movieStore.allMovies[indexPath.row]
            
            // Remove the item from the item store
            movieStore.removeMovie(movie)
            
            // Also remove that row from the table view with an animation
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func tableView(_ tableView: UITableView,
                            moveRowAt sourceIndexPath: IndexPath,
                            to destinationIndexPath: IndexPath) {
        // Update the model
        movieStore.moveMovie(from: sourceIndexPath.row, to: destinationIndexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        let movie = movieStore.allMovies[indexPath.row]
        
        cell.textLabel?.text = movie.title
        
            if let rating = movie.rating
            {
                switch rating {
                case 1-3:
                    cell.imageView?.image = #imageLiteral(resourceName: "red")
                case 4-6:
                    cell.imageView?.image = #imageLiteral(resourceName: "yellow")
                case 7-10:
                    cell.imageView?.image = #imageLiteral(resourceName: "green")
                default:
                    cell.imageView?.image = #imageLiteral(resourceName: "blank")
                }
            }
            else
            {
                cell.imageView?.image = #imageLiteral(resourceName: "blank")
            }
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 65
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If the triggered segue is the "showItem" segue
        switch segue.identifier {
        case "showMovie"?:
            // Figure out which row was just tapped
            if let row = tableView.indexPathForSelectedRow?.row {
                // Get the item associated with this row and pass it along
                let movie = movieStore.allMovies[row]
                let detailViewController = segue.destination as! DetailViewController
                detailViewController.movie = movie
            }
        default:
            preconditionFailure("Unexepected segue identifier.")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tableView.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        navigationItem.leftBarButtonItem = editButtonItem
    }
}
