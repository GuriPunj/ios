//
//  DetailViewController.swift
//  Movie Tracker
//
//  Created by Gurpreet Singh Punj on 2017-12-16.
//  Copyright © 2017 Gurpreet Singh Punj. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController
{
    @IBOutlet var titleField: UITextField!
    @IBOutlet var ratingField: UITextField!
    @IBOutlet var theatreField: UITextField!
    @IBOutlet var datePicker: UIDatePicker!
    
    var movie: Movie! {
        didSet {
            navigationItem.title = movie.title
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        titleField.text = movie.title
        ratingField.text = numberFormatter.string(from: NSNumber(value: movie.rating!))
        theatreField.text = movie.theatre
        datePicker.date = movie.viewedDate
    }
    
    let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimum = 1
        formatter.maximum = 10
        return formatter
    }()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
        // "Save" changes to item
        movie.title = titleField.text ?? ""
        movie.theatre = theatreField.text!
        movie.viewedDate = datePicker.date
        
        if let valueText = ratingField.text,
            let value = numberFormatter.number(from: valueText) {
            movie.rating = value.intValue
        } else {
            movie.rating = 1
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

}
