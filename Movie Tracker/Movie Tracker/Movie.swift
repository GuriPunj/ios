//
//  Movie.swift
//  Movie Tracker
//
//  Created by Gurpreet Singh Punj on 2017-12-16.
//  Copyright © 2017 Gurpreet Singh Punj. All rights reserved.
//

import UIKit

class Movie: NSObject, NSCoding
{
    var title: String
    var rating: Int?
    var theatre: String
    var viewedDate: Date
    
    init(title: String, theatre: String, rating: Int?, viewedDate: Date) {
        self.theatre = theatre
        self.rating = rating
        self.viewedDate = viewedDate
        self.title = title
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(viewedDate, forKey: "viewedDate")
        aCoder.encode(theatre, forKey: "theatre")
        aCoder.encode(rating, forKey: "rating")
    }
    
    required init?(coder aDecoder: NSCoder) {
        title = aDecoder.decodeObject(forKey: "title") as! String
        viewedDate = aDecoder.decodeObject(forKey: "viewedDate") as! Date
        theatre = aDecoder.decodeObject(forKey: "theatre") as! String
        rating = aDecoder.decodeObject(forKey: "rating") as! Int?
        super.init()
    }
}
